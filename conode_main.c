/****************************************************************************
 * canutils/conode/conode_main.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <nuttx/drivers/can.h>

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/slave.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private types
 ****************************************************************************/

/****************************************************************************
 * Global variables
 ****************************************************************************/

struct txcb_priv_s
{
  int fd;
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************/
static int txcb(FAR void *priv, FAR struct canmsg_s *msg)
{
  FAR struct txcb_priv_s *privdata = (FAR struct txcb_priv_s*)priv;
  struct can_msg_s nmsg;
  nmsg.cm_hdr.ch_dlc = msg->dlc;
  nmsg.cm_hdr.ch_id  = msg->id;
#ifdef CONFIG_CAN_EXTID
  nmsg.cm_hdr.ch_extid = 0;
#endif
  memcpy(nmsg.cm_data, msg->data, 8);
  return write(privdata->fd, &nmsg, CAN_MSGLEN(msg->dlc));
}

/****************************************************************************/
static void usage(void)
{
  fprintf(stderr, 
          "conode <nodeid> <candevice>\n"
         );
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int conode_main(int argc, FAR char *argv[])
#endif
{
  struct canopen_slave_s node;
  struct can_msg_s msgbuf;
  struct canmsg_s msg;
  struct txcb_priv_s priv;
  unsigned long nodeid;
  int fd;
  int ret;

  /* Open the character driver that wraps the can interface */

  if(argc < 3)
    {
      usage();
      return 1;
    }

  /* Open CAN device */

  fd = open(argv[2], O_RDWR);
  if(fd < 0)
    {
      fprintf(stderr, "Failed to open CAN device (errno=%d)\n",errno);
      return ERROR;
    }
  priv.fd = fd;

  /* Decode node id */

  set_errno(0);
  nodeid = strtoul(argv[1], NULL, 0);
  if(errno != 0)
    {
      fprintf(stderr, "Invalid node ID\n");
      close(fd);
      return ERROR;
    }
  /* Initialize CANopen node */

  ret = canopen_slave_init(&node, nodeid, txcb, &priv);

  if(ret)
    {
      fprintf(stderr, "Node init failed\n");
      close(fd);
      return ERROR;
    }

  fprintf(stderr, "Node loop starting\n");

  do
    {
      ret = read(fd, &msgbuf, sizeof(msgbuf));
      if(ret>0)
        {
          msg.id  = msgbuf.cm_hdr.ch_id;
          msg.dlc = msgbuf.cm_hdr.ch_dlc;
          memcpy(msg.data, msgbuf.cm_data, 8);
          canopen_slave_rx(&node, &msg);
        }
    }
  while(ret>0);

  fprintf(stderr, "Node loop terminated\n");

  close(fd);

  return 0;
}

