conode - CANopen slave demo and test driver

This software is a demonstration CANopen slave node that implements the default communication profile. It can be used as a skeleton for a more complex CANopen slave node.

execute as:

    conode <nodeid> <candevice>

example:

    conode 1 /dev/can0

For the moment there are no persistent settings. The node boots with the default communication profile and predefined SDO/PDOs.